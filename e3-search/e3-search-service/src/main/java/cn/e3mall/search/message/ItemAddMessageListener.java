package cn.e3mall.search.message;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;

import cn.e3mall.common.pojo.SearchItem;
import cn.e3mall.search.mapper.ItemMapper;
/**
 * 监听商品添加消息,接收到消息后将对应的商品信息对应到索引库
 * @author shanqingyun
 *
 */
public class ItemAddMessageListener implements MessageListener {
	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private SolrServer solrServer;
	@Override
	public void onMessage(Message arg0) {
		//从消息中得到商品id
		TextMessage textMessage=(TextMessage) arg0;
		try {
			long itemId=Long.parseLong(textMessage.getText());
			//等待事务提交
			Thread.sleep(1000);
			//根据商品id 查询商品信息
			SearchItem searchItem = itemMapper.getItemById(itemId);
			//创建一个文档对象
			SolrInputDocument document=new SolrInputDocument();
			//向文档对象中添加域
			document.addField("id", searchItem.getId());
			document.addField("item_title", searchItem.getTitle());
			document.addField("item_sell_point", searchItem.getSell_point());
			document.addField("item_price", searchItem.getPrice());
			document.addField("item_image", searchItem.getImages());
			document.addField("item_category_name", searchItem.getCategory_name());
			//把文档写入索引库
			solrServer.add(document);
			//提交
			solrServer.commit();
		}catch (Exception e){
			e.printStackTrace();
		}

	}

}

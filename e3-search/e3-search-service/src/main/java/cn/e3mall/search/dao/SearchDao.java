package cn.e3mall.search.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.e3mall.common.pojo.SearchItem;
import cn.e3mall.common.pojo.SearchResult;

/**
 * 商品搜索dao
 * @author shanqingyun
 *
 */
@Repository
public class SearchDao {
	@Autowired
	private SolrServer solrServer;
	public SearchResult search(SolrQuery query) throws Exception{
		//根据query查询索引库
		QueryResponse response=solrServer.query(query);
		//取查询结果
		SolrDocumentList results = response.getResults();
		//取查询结果总记录数
		long numFound = results.getNumFound();
		SearchResult searchResult=new SearchResult();
		searchResult.setRecordCount(numFound);
		Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();
		List<SearchItem> searchLists=new ArrayList<SearchItem>();
		//取商品列表,需要取高亮结果
		for (SolrDocument solrDocument : results) {
			SearchItem item=new SearchItem();
			item.setCategory_name((String) solrDocument.get("item_category_name"));
			item.setId((String) solrDocument.get("id"));
			item.setImage((String) solrDocument.get("item_image"));
			item.setPrice((long) solrDocument.get("item_price"));
			item.setSell_point((String) solrDocument.get("item_sell_point"));
			//取高亮相熟
			List<String> list = highlighting.get(solrDocument.get("id")).get("item_title");
			String title="";
			if(list!=null&&list.size()>0){
				title=list.get(0);
			}else{
				title=(String)solrDocument.get("item_title");
			}
			
			item.setTitle(title);
			searchLists.add(item);
		}
		//返回结果
		searchResult.setItemList(searchLists);
		return searchResult;
	}
}

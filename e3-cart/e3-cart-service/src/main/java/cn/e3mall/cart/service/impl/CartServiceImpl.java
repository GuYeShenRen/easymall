package cn.e3mall.cart.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cn.e3mall.cart.service.CartService;
import cn.e3mall.common.jedis.JedisClient;
import cn.e3mall.common.util.E3Result;
import cn.e3mall.common.util.JsonUtils;
import cn.e3mall.mapper.TbItemMapper;
import cn.e3mall.pojo.TbItem;
@Service
public class CartServiceImpl implements CartService {
	@Autowired
	private JedisClient jedisClient;
	@Value("${REDIS_CART_PRE}")
	private String REDIS_CART_PRE;
	@Autowired
	private TbItemMapper itemMapper;
	@Override
	public E3Result addCart(long userId, long itemId,int num) {
		//向redis中添加购物车
		//数据类型hash ,key 用户ID field 商品ID value 商品信息
		//判断商品是否存在,数量相加
		Boolean hexists = jedisClient.hexists(REDIS_CART_PRE+":"+userId, itemId+"");
		if(hexists){
			String json = jedisClient.hget(REDIS_CART_PRE+":"+userId, itemId+"");
			TbItem item = JsonUtils.jsonToPojo(json, TbItem.class);
			item.setNum(item.getNum()+num);
			//写回redis
			jedisClient.hset(REDIS_CART_PRE+":"+userId, itemId+"", JsonUtils.objectToJson(item));
			return E3Result.ok();
		}
		//商品不存在根据商品ID取商品信息
		TbItem tbItem = itemMapper.selectByPrimaryKey(itemId);
		//添加数量信息
		tbItem.setNum(num);
		String image = tbItem.getImage();
		if(StringUtils.isNotBlank(image)){
			tbItem.setImage(image.split(",")[0]);
		}
		//添加到购物车列表
		jedisClient.hset(REDIS_CART_PRE+":"+userId, itemId+"", JsonUtils.objectToJson(tbItem));
		//返回成功
		return E3Result.ok();
	}
	
	/**
	 * 合并购物车
	 */
	@Override
	public E3Result mergeCart(long userId, List<TbItem> list) {
		// 遍历商品列表
		//将商品添加到购物车
		//判断购物车中是否有此商品
		//有则合并数量
		//没有则添加
		for (TbItem tbItem : list) {
			addCart(userId, tbItem.getId(), tbItem.getNum());
		}
		//返回成功
		return E3Result.ok();
	}
	/**
	 * 取购物车列表
	 */
	@Override
	public List<TbItem> getCartList(long userId) {
		//根据用户ID查询购物车列表
		List<String> jsonList = jedisClient.hvals(REDIS_CART_PRE+":"+userId);
		List<TbItem> list=new ArrayList<TbItem>();
		for (String string : jsonList) {
			TbItem item = JsonUtils.jsonToPojo(string, TbItem.class);
			list.add(item);
		}
		return list;
	}
	/**
	 * 更新购物车的商品的数量
	 */
	@Override
	public E3Result updateCartNum(long userId, long itemId, int num) {
		//从redis中取商品信息
		String json = jedisClient.hget(REDIS_CART_PRE+":"+userId, itemId+"");
		TbItem item=JsonUtils.jsonToPojo(json, TbItem.class);
		//更新数量
		item.setNum(num);
		//写入redis
		jedisClient.hset(REDIS_CART_PRE+":"+userId, itemId+"", JsonUtils.objectToJson(item));
		return E3Result.ok();
	}
	/**
	 *删除购物车商品
	 */
	@Override	
	public E3Result deleteCartItem(long userId, long itemId) {
		jedisClient.hdel(REDIS_CART_PRE+":"+userId, itemId+"");
		return E3Result.ok();
	}
	/**
	 * 删除购物车信息
	 */
	@Override
	public E3Result clearCartItem(long userId) {
		//删除购物车信息
		jedisClient.del(REDIS_CART_PRE+":"+userId);
		
		return E3Result.ok();
	}

}

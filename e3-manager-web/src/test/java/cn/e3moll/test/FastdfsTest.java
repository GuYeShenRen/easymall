package cn.e3moll.test;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.junit.Test;

public class FastdfsTest {
	@Test
	public void uploadTest() throws Exception{
		//1.创建一个配置文件,文件中的内容就是tracker服务的地址
		ClientGlobal.init("D:/learning/java/HomeWork/Workspace/e3-manager-web/src/main/resources/conf/client.conf");
		//2.创建一个trackerclient对象,直接new 一个
		TrackerClient trackerClient = new TrackerClient();
		//3.使用trackerclient对象创建连接,获得一个TrackerServer对象
		TrackerServer trackerServer =trackerClient.getConnection();
		//4.创建一个storageServer的引用,值为null
		StorageServer storageServer=null;
		//5.创建一个storageClient对象,需要两个参数TrackerServer对象,StorageServer的引用
		StorageClient storageClient=new StorageClient(trackerServer, storageServer);
		//6.使用StorageClient对象上传图片
		//扩张名不带"."
		String[] strings=storageClient.upload_file("G:/9a80e2d06170b6bb01046233ede701b3.jpg","jpg",null);
		for (String string : strings) {
			System.out.println(string);
		}
		
	}
}

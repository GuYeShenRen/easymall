package cn.e3mall.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.e3mall.common.util.E3Result;
import cn.e3mall.content.service.ContentService;
import cn.e3mall.pojo.TbContent;
/**
 * 商品内容管理Controller
 * @author shanqingyun
 *
 */
@Controller
public class ContentController {
	@Autowired
	private ContentService contentService;
	/**
	 * 内容管理添加
	 * @param tbContent
	 * @return
	 */
	@RequestMapping(value="/content/save",method=RequestMethod.POST)
	@ResponseBody
	public E3Result addContent(TbContent tbContent){
		E3Result result = contentService.addContent(tbContent);
		
		return result;
	}
}

package cn.e3mall.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.e3mall.common.util.E3Result;
import cn.e3mall.search.service.SearchItemService;
import cn.e3mall.service.ItemService;
/**
 * 控制索引添加controller
 * @author shanqingyun
 *
 */
@Controller
public class SearchItemController {
	
	@Autowired
	private SearchItemService searchItemService;
	@RequestMapping("/index/item/import")
	@ResponseBody
	public E3Result importItem(){
		E3Result result=searchItemService.importAllItems();
		return result;
	}
	
}

package cn.e3mall.sso.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import cn.e3mall.common.util.E3Result;
import cn.e3mall.mapper.TbUserMapper;
import cn.e3mall.pojo.TbUser;
import cn.e3mall.pojo.TbUserExample;
import cn.e3mall.pojo.TbUserExample.Criteria;
import cn.e3mall.sso.service.RegisterService;
/**
 * 用户注册service
 * @author shanqingyun
 *
 */
@Service
public class RegisterServiceImpl implements RegisterService {
	@Autowired
	private TbUserMapper userMapper;
	@Override
	public E3Result checkData(String param, int type) {
		// 根据不同的type 生成不同的查询条件
		TbUserExample example=new TbUserExample();
		Criteria criteria = example.createCriteria();
		//1.用户名2.手机号3.邮箱
		if(type==1){
			criteria.andUsernameEqualTo(param);
		}else if(type==2){
			criteria.andPhoneEqualTo(param);
		}else if(type==3){
			criteria.andEmailEqualTo(param);
		}else{
			return E3Result.build(400, "数据类型错误");
		}
		//执行查询
		List<TbUser> list = userMapper.selectByExample(example);
		//判断结果中是否包含数据
		//有数据返回false
		if(list!=null&&list.size()>0){
			return E3Result.ok(false);
		}
		//没有数据返回true
		return E3Result.ok(true);
	}
	@Override
	public E3Result register(TbUser tbUser) {
		//数据有效性校验
		if(StringUtils.isBlank(tbUser.getUsername())||StringUtils.isBlank(tbUser.getPassword())||StringUtils.isBlank(tbUser.getPhone())){
			return E3Result.build(400, "用户数据不完整注册失败");
		}
		//1.用户名2.手机号
		E3Result result = checkData(tbUser.getUsername(), 1);
		if(!(boolean) result.getData()){
			return E3Result.build(400, "用户名重复");
		}
		E3Result resultPhone = checkData(tbUser.getPhone(), 2);
		if(!(boolean) result.getData()){
			return E3Result.build(400, "手机号码已被占用");
		}
		//补全pojo的属性
		tbUser.setCreated(new Date());
		tbUser.setUpdated(new Date());
		//password MD5加密
		String md5Password = DigestUtils.md5DigestAsHex(tbUser.getPassword().getBytes());
		tbUser.setPassword(md5Password);
		//用户数据插入到数据库
		userMapper.insert(tbUser);
		//返回添加成功
		return E3Result.ok();
	}

}

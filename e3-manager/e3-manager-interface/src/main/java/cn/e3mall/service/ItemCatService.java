package cn.e3mall.service;

import java.util.List;

import cn.e3mall.common.pojo.EasyUITreeNode;

public interface ItemCatService {
	//通过parentId查询子节点
	List<EasyUITreeNode> getItemCatList (long parentId);
}
